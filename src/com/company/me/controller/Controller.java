package com.company.me.controller;

import com.company.me.model.Model;
import com.company.me.model.NotUniqueLoginException;
import com.company.me.model.Regex;
import com.company.me.view.TEXT_CONSTANTS;
import com.company.me.view.View;

import java.util.Scanner;

public class Controller {
Scanner sc =new Scanner(System.in);
View view;
Model model;

public Controller(View view, Model model){
    this.view =view;
    this.model=model;
}

    public void initiation(){   //Start
        //for (int i = 0; i <2 ; i++) {
        model.setFName(input(View.bundle.getString(TEXT_CONSTANTS.FIRST_NAME), Regex.FName));    //
    getLogin();// }//
       // System.out.println(model.getFName());
       // System.out.println( model.getLogin());
    }

   /* public void nameChecker (String name){
    String fname=name;
        for (String a: model.getFName()) {

            if (a.equals(fname)){
                view.print_message("already exists");       //
                fname=input(View.bundle.getString(TEXT_CONSTANTS.FIRST_NAME), Regex.FName);       //
            }

        }
        model.setFName(fname);
    }*/
public void getLogin(){
    String login;
    login=input(View.bundle.getString(TEXT_CONSTANTS.LOGIN_DATA), Regex.login);
    try{
        loginChecker(login);
    }
    catch (NotUniqueLoginException e){
        view.print_message(View.bundle.getString(TEXT_CONSTANTS.LOGIN_EXIST));
        getLogin();
    }
    model.setLogin(login);
}
    public void loginChecker (String login) throws NotUniqueLoginException {
        String loginn=login;
        for (String a: model.getLogin()) {
            if (a.equals(loginn)){

               // loginn=input(View.bundle.getString(TEXT_CONSTANTS.LOGIN_DATA), Regex.login);
                throw new NotUniqueLoginException("Login not unique",a) ;
            }

        }
        //model.setLogin(loginn);
    }



    public String input(String message, String regex){
        String ret ;
        view.print_message(message);

        while( !(sc.hasNext() && (ret = sc.next()).matches(regex))) {
            view.wrongInput(message);
        }

        return ret;
    }

}
