package com.company.me.view;

public interface TEXT_CONSTANTS {

    String INPUT_STRING_DATA = "input.string.data";
    String FIRST_NAME = "input.first.name.data";
    String WRONG_INPUT_DATA = "input.wrong.data";
    String LOGIN_DATA = "input.login.data";
    String LOGIN_EXIST = "input.login.exist";
}
