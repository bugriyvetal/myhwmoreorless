package com.company.me;

import com.company.me.controller.Controller;
import com.company.me.model.Model;
import com.company.me.view.View;

public class Main {

    public static void main(String[] args) {
        Controller c = new Controller(new View(),new Model());
        c.initiation();
    }
}
